/****** Object:  StoredProcedure [dbo].[sp_MailNotif_GetMailContent]    Script Date: 07/03/2019 05:19:38 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MailNotif_GetMailContent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MailNotif_GetMailContent]
GO
/****** Object:  StoredProcedure [dbo].[sp_MailNotif_GetClienteSaldoVencido]    Script Date: 07/03/2019 05:19:38 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MailNotif_GetClienteSaldoVencido]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MailNotif_GetClienteSaldoVencido]
GO
/****** Object:  StoredProcedure [dbo].[sp_MailNotif_AddNotificacion]    Script Date: 07/03/2019 05:19:38 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MailNotif_AddNotificacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MailNotif_AddNotificacion]
GO
/****** Object:  Table [dbo].[MailNotif_MailContentSV]    Script Date: 07/03/2019 05:19:38 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MailNotif_MailContentSV]') AND type in (N'U'))
DROP TABLE [dbo].[MailNotif_MailContentSV]
GO
/****** Object:  Table [dbo].[MailNotif_ClienteSaldoVencido]    Script Date: 07/03/2019 05:19:38 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MailNotif_ClienteSaldoVencido]') AND type in (N'U'))
DROP TABLE [dbo].[MailNotif_ClienteSaldoVencido]
GO
/****** Object:  Table [dbo].[MailNotif_ClienteSaldoVencido]    Script Date: 07/03/2019 05:19:38 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MailNotif_ClienteSaldoVencido](
	[CardCode] [varchar](50) NOT NULL,
	[CardName] [varchar](250) NULL,
	[Mail] [varchar](150) NULL,
	[FechaUltimaNotificacion] [datetime] NULL,
	[Notificaciones] [int] NULL,
 CONSTRAINT [PK_MailNotif_ClienteSaldoVencido] PRIMARY KEY CLUSTERED 
(
	[CardCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MailNotif_MailContentSV]    Script Date: 07/03/2019 05:19:39 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MailNotif_MailContentSV](
	[MailConfigID] [int] NOT NULL,
	[MailFrom] [varchar](150) NULL,
	[MailSubject] [varchar](50) NULL,
	[HTMLBody] [varchar](max) NULL,
	[RecursoImagen] [varchar](max) NULL,
	[RutaImagenes] [varchar](250) NULL,
 CONSTRAINT [PK_MailNotif_MailContentSV] PRIMARY KEY CLUSTERED 
(
	[MailConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_MailNotif_AddNotificacion]    Script Date: 07/03/2019 05:19:39 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_MailNotif_AddNotificacion]
	 @pCardCode varchar(50)
	,@pCardName varchar(250) 
	,@pMail varchar(150)
as
begin

	if not exists(select * from mailnotif_clientesaldovencido where CardCode = @pCardCode)
	begin

		insert into 
			mailnotif_clientesaldovencido
				(
					 CardCode
					,CardName
					,Mail
					,FechaUltimaNotificacion
					,Notificaciones					
				)
		values
				(
					 @pCardCode
					,@pCardName
					,@pMail
					,GETDATE()
					,1
				)
	end
	else
	begin
		declare @nNotif int
		set @nNotif = (select ISNULL(Notificaciones, 1) from mailnotif_clientesaldovencido where CardCode = @pCardCode) + 1

		update 
			mailnotif_clientesaldovencido
		set
			 CardName = @pCardName
			,Mail = @pMail
			,FechaUltimaNotificacion = GETDATE()
			,Notificaciones = @nNotif
		where
			CardCode = @pCardCode
	end

end
GO
/****** Object:  StoredProcedure [dbo].[sp_MailNotif_GetClienteSaldoVencido]    Script Date: 07/03/2019 05:19:39 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_MailNotif_GetClienteSaldoVencido]
AS
BEGIN
	
	select distinct 
		 C.CardCode CardCode
		,C.CardName CardName
		,C.E_Mail Mail
	from 
		Eximagen_NuevaB.dbo.JDT1 S
		inner join Eximagen_NuevaB.dbo.OCRD C
			on C.CardCode = S.ShortName
			and C.E_Mail <> ''
			and C.E_Mail is not null
			and C.cardcode not in (select 
									cardcode collate Traditional_Spanish_CI_AS as cardcode
								from
									MailNotif_ClienteSaldoVencido
								where 
									DATEDIFF(d,FechaUltimaNotificacion, GETDATE()) < 7
							)
	where 
		S.TransType = 13
		and len(S.ShortName) = 5 
		and S.BalDueDeb > 0
		and DATEDIFF(day, S.DueDate, GETDATE()) >= 30 
		

END
GO
/****** Object:  StoredProcedure [dbo].[sp_MailNotif_GetMailContent]    Script Date: 07/03/2019 05:19:39 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[sp_MailNotif_GetMailContent]
	@pMailConfig INT = 0
AS
BEGIN

	SELECT 
		 MailConfigID
		,MailFrom
		,MailSubject
		,HTMLBody
		,RecursoImagen
		,RutaImagenes
	FROM
		MailNotif_MailContentSV
	WHERE
		(MailConfigID = @pMailConfig OR 0 = @pMailConfig)
END
GO

INSERT [dbo].[MailNotif_MailContentSV] ([MailConfigID], [MailFrom], [MailSubject], [HTMLBody], [RecursoImagen], [RutaImagenes]) VALUES (1, N'notificaciones@promoline.com.mx', N'Notificacion Saldo Vencido', N'<html>       
    <head>            
             <style type=\"text/css\">body { font-family:Verdana, Geneva, sans-serif; }             
            </style>
      </head>            
          
            <body>                
                <table width=\"600\">                    
                    <tr>
                        <td>                            
                            <img src=cid:estado_de_cuenta width=\"596\" height=\"95\" alt=\"Promoline\" />                            
                            <p>
								Estimado Cliente, su cuenta fue bloqueada derivado de que tiene saldos vencidos.  
								<br>
								<br>
								Por favor comunicarse con nuestra área de Crédito.                             
                            </p>    
                            <img src=cid:linea_firma />                           
                            <p><img src=cid:cel_firma alt=\"Promoline\/>    
                                  <span style=''font-family:"Helvetica",sans-serif;mso-bidi-font-family: Helvetica;color:#7F7F7F''>&nbsp;Tel: 52 (55) 5278-0180          
                                      <o:p>        
                                      </o:p>    
                                  </span>   
                            </p>
                        </td>
                    <tr>
                        <td>
                             <p><img src=cid:ubic_firma alt=\"Promoline\/>        
                                <span style=''font-family:"Helvetica",sans-serif;mso-bidi-font-family: Helvetica;color:#7F7F7F''>&nbsp;Toltecas No. 139, Col. San Pedro de los Pinos, Del. Álvaro Obregón, C.P. 01180, CDMX.         
                                </span>        
                                <span style=''font-family:-webkit-standard; color:black''>            
                                    <o:p>            
                                    </o:p>        
                                </span>    
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <p><img src=cid:page_firma" alt=\"Promoline\/>        
                                <span>            
                                    <span style=''font-family:"Helvetica",sans-serif; mso-bidi-font-family:Helvetica;color:#7F7F7F''>                
                                        <a href="http://www.promoline.com.mx/eshop/Homepromo.php">                    
                                            <span style=''color:#7F7F7F''>&nbsp;promoline.mx                     
                                            </span></a>            
                                    </span>        
                                </span>        
                                <span style=''font-family:-webkit-standard;color:black''>            
                                    <o:p>            
                                    </o:p>        
                                </span>    
                            </p>    
                        </td>
                    </tr>
                </table>
            </body>        
</html>', N'estado_de_cuenta.jpg;linea_firma.jpg;cel_firma.jpg;ubic_firma.jpg;page_firma.jpg', N'C:\Resource_MailSaldoCliente')
GO
