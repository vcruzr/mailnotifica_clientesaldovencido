﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class DataLog
    {
        public TipoArchivo TipoArchivo { get; set; }
        public TipoLog TipoLog { get; set; }

        public string sModulo = "MailNot_SaldoVencido"; 
        public string sClase { get; set; }
        public string sMetodo { get; set; }
        public string sMensaje { get; set; }

    }
}


public enum TipoLog
{
    Evento = 1,
    Error = 2
}

public enum TipoArchivo
{
    Xml = 1,
    Txt = 2
}
