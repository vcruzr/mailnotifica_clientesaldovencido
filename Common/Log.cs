﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Configuration;
using MailNot_Saldos.Entidades;

namespace MailNot_Saldos.Common
{
    public class Log: IDisposable
    {
        StringBuilder sb = new StringBuilder();
        private string strRutaLog = string.Empty;

        public Log()
        {
            try
            {
                strRutaLog = ConfigurationManager.AppSettings["LogFile"].ToString() + "_";
                strRutaLog = strRutaLog + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void WriteLog(DataLog Datos)
        {
            try
            {
                strRutaLog = strRutaLog.Replace(".xml", "");
                strRutaLog = strRutaLog.Replace(".txt", "");
                switch (Datos.TipoArchivo)
                {
                    case TipoArchivo.Xml:
                        writeLogXML(Datos);
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void writeLogXML(DataLog DatosLog)
        {
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(strRutaLog)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strRutaLog));
                }
                if (!File.Exists(strRutaLog))
                {
                    XDocument ixDoc = new XDocument(
                        new XElement("Eventos",
                            new XElement("Evento"),
                            new XElement("Error")
                    ));
                    ixDoc.Save(strRutaLog);
                }
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strRutaLog);
                XmlNode NodeMenu = xmlDoc.SelectSingleNode("Eventos");
                XmlNode NodeSubmenu = NodeMenu.SelectSingleNode(DatosLog.TipoLog.ToString());
                XmlNode nNode = xmlDoc.CreateNode(XmlNodeType.Element, DatosLog.TipoLog.ToString() + "Log", null);
                XmlAttribute _FechaHora = xmlDoc.CreateAttribute("FechaHora");
                _FechaHora.Value = DateTime.Now.ToString();
                XmlAttribute _Evento = xmlDoc.CreateAttribute("Evento");
                _Evento.Value = DatosLog.TipoLog.ToString();
                XmlAttribute _Modulo = xmlDoc.CreateAttribute("Modulo");
                _Modulo.Value = DatosLog.sModulo;
                XmlAttribute _Clase = xmlDoc.CreateAttribute("Clase");
                _Clase.Value = DatosLog.sClase;
                XmlAttribute _Metodo = xmlDoc.CreateAttribute("Metodo");
                _Metodo.Value = DatosLog.sMetodo;
                XmlAttribute _Mensaje = xmlDoc.CreateAttribute("Mensaje");
                _Mensaje.Value = DatosLog.sMensaje;
                nNode.Attributes.Append(_FechaHora);
                nNode.Attributes.Append(_Evento);
                nNode.Attributes.Append(_Modulo);
                nNode.Attributes.Append(_Clase);
                nNode.Attributes.Append(_Metodo);
                nNode.Attributes.Append(_Mensaje);
                NodeSubmenu.AppendChild(nNode);
                xmlDoc.Save(strRutaLog);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {

            Dispose(true);

            GC.SuppressFinalize(this);

        }

        ~Log()
        {
            Dispose(false);
        }

        #endregion    



    }
}
