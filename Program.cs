﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailNot_Saldos.Saldo;

namespace MailNot_Saldos
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Saldos Saldo = new Saldos())
            {
                Console.WriteLine("INICIA ENVIO DE NOFICACIONES CLIENTES CON SALDO VENCIDO...");
                Saldo.NotificarSaldosVencidos();
                Console.WriteLine("FINALIZA PROCESO.");
            }
        }
    }
}
