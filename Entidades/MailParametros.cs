﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class MailParametros
    {
        public int Port { get; set; }
        public string Host { get; set; }
        public bool EnableSSL { get; set; }
        public int TimeOut { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string User { get; set; }
        public string Passwd { get; set; }
        public string AliasFrom { get; set; }

    }
}
