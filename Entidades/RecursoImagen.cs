﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class RecursoImagen
    {
        public string RutaImagen { get; set; }
        public string NombreImagen { get; set; }
    }
}
