﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class GeneralResult
    {
        public int CodigoResultado { get; set; }
        public string DescripcionResultado { get; set; }
        public ResultType TipoResultado { get; set; }        
        public object DataResultado { get; set; }
    }

    public enum ResultType
    {
        Exito = 1,
        Error = 2
    }
}
