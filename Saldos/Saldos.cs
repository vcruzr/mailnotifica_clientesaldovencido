﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailNot_Saldos.Entidades;
using MailNot_Saldos.Common;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf.draw;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.xml;
using MailNot_Saldos.eMail;
using System.Configuration;
using System.Net.Mail;

namespace MailNot_Saldos.Saldo
{
    public class Saldos : IDisposable
    {
        string _pdfDirectory = string.Empty;

        string _Mensaje = string.Empty;
        const string _Clase = "Saldos";
        const string _Modulo = "MailNot_Saldos";

        const string SP_CMD_CLIENTE_SALDO_VENCIDO = "sp_MailNotif_GetClienteSaldoVencido";
        const string SP_CMD_DATOS_SALDO_CLIENTE = "SP_DatosAdeudosCliente";
        const string SP_CMD_ESTADOS_SALDO_CLIENTE = "SP_EstadosAdeudosCliente";
        const string SP_CMD_TOTALES_SALDO_CLIENTE = "SP_TotalesEstadoAdeudosCliente";
        const string SP_CMD_MAIL_CONTENT = "sp_MailNotif_GetMailContent";
        const string SP_CMD_REGISTRA_NOTIFICACION = "sp_MailNotif_AddNotificacion";

        public Saldos()
        {
            _pdfDirectory = ConfigurationManager.AppSettings["pdfDir"].ToString();
        }

        public GeneralResult NotificarSaldosVencidos()
        {
            GeneralResult gr = new GeneralResult();
            GeneralResult clientesVencidos = new GeneralResult();
            List<ClienteSaldoVencido> lst_csv = new List<ClienteSaldoVencido>();
            int nList = 0; 
            try
            {
                Console.WriteLine("OBTENIENDO CLIENTES.");
                clientesVencidos = GetClienteSaldoVencido();
                lst_csv = (List<ClienteSaldoVencido>)clientesVencidos.DataResultado;
                Console.Write("SE ENCONTRARON " +  lst_csv.Count.ToString() + "CLIENTES.");
                foreach (ClienteSaldoVencido csv in lst_csv)
                {
                    nList += 1; 
                    Console.WriteLine("OBTENIENDO INFORMACION DEL CLIENTE " + nList.ToString() + " - " + csv.CardCode + " : " + csv.CardName);
                    gr = EnviarNotificacion(csv);
                    if(gr.TipoResultado == ResultType.Exito)
                    {
                        Console.WriteLine("NOTIFICACION ENVIADA CON EXITO.");
                        gr = RegistraNotificacion(csv);
                        gr.DescripcionResultado = "Se envio registro Notificacion Exitosa para Cliente: " + csv.CardCode + "-" + csv.CardName + " Mail:" + csv.EMail;
                        DataLog dataLog = new DataLog() { TipoLog = TipoLog.Evento, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "NotificarSaldosVencidos()", sMensaje = gr.DescripcionResultado };
                        using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                        if (gr.TipoResultado == ResultType.Exito)
                        {
                            gr.TipoResultado = ResultType.Exito;
                            gr.DescripcionResultado = "Se envio Envio Notificacion Exitosa para Cliente: " + csv.CardCode + "-" + csv.CardName + " Mail:" + csv.EMail;
                            dataLog = new DataLog() { TipoLog = TipoLog.Evento, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "NotificarSaldosVencidos()", sMensaje = gr.DescripcionResultado };
                            using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                        }
                        else
                        {
                            _Mensaje = "No se Pudo Registrar Notificacion en DB";
                            dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "NotificarSaldosVencidos()", sMensaje = "Error: " + _Mensaje };
                            using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                        }
                    }
                    else
                    {
                        Console.WriteLine("SE PRESENTARON PROBLEMAS PARA ENVIAR NOTIFICACION DEL CLIENTE " + nList.ToString() + " - " + csv.CardCode + " : " + csv.CardName);
                        _Mensaje = "Error al Enviar Notificacion Saldo Vencido, Cliente: " + csv.CardCode + "-" + csv.CardName + " Mail:" + csv.EMail;
                        DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "NotificarSaldosVencidos()", sMensaje = "Error: " + _Mensaje };
                        using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                    }
                }
                return gr;
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "NotificarSaldosVencidos()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                gr = null;
            }
            return gr;
        }

        private GeneralResult EnviarNotificacion(ClienteSaldoVencido Cliente)
        {
            GeneralResult gr = new GeneralResult();
            GeneralResult Contenido = new GeneralResult();
            try
            {
                Contenido = ContenidoMail(Cliente);
                if(Contenido.TipoResultado == ResultType.Exito)
                {
                    using(Mail email = new Mail())
                    {
                        gr = email.EnviarMail((MailContent)Contenido.DataResultado);
                    }   
                }
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "EnviarNotificacion()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            return gr;
        }

        private GeneralResult GetClienteSaldoVencido()
        {
            GeneralResult gr = new GeneralResult();
            List<ClienteSaldoVencido> lst_csv = new List<ClienteSaldoVencido>();
            ClienteSaldoVencido csv;
            DataTable dt = null;
            try
            {
                using (AccesoDatos dCliente = new AccesoDatos(DBConnection.Eximagen_Electro))
                {
                    dt = dCliente.ExecuteDataTable(SP_CMD_CLIENTE_SALDO_VENCIDO);
                }

                if(dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        csv = new ClienteSaldoVencido() {
                            CardCode = Convert.ToString(dr["CardCode"]),
                            CardName = Convert.ToString(dr["CardName"]),
                            EMail = Convert.ToString(dr["Mail"])
                        };
                        lst_csv.Add(csv);
                    }
                }

                gr.CodigoResultado = 1;
                gr.DescripcionResultado = "Lista Clientes Saldo Vencido Obtenida";
                gr.TipoResultado = ResultType.Exito;
                gr.DataResultado = lst_csv;
                return gr;
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "GetClienteSaldoVencido()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                lst_csv = null;
                csv = null;
                dt = null;
            }
            return gr;
        }

        private GeneralResult RegistraNotificacion(ClienteSaldoVencido Cliente)
        {
            GeneralResult gr = new GeneralResult();
            bool exito = false;
            try
            {
                using (AccesoDatos data = new AccesoDatos(DBConnection.Eximagen_Electro))
                {
                    data.AddParameterIn("@pCardCode", SqlDbType.VarChar, Cliente.CardCode);
                    data.AddParameterIn("@pCardName", SqlDbType.VarChar, Cliente.CardName);
                    data.AddParameterIn("@pMail", SqlDbType.VarChar, Cliente.EMail);
                    exito = data.ExecuteQuery(SP_CMD_REGISTRA_NOTIFICACION); 
                }

                if(exito)
                {
                    gr.CodigoResultado = 1;
                    gr.DescripcionResultado = "Notificacion Registrada en DB";
                    gr.TipoResultado = ResultType.Exito;
                }
                return gr;
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "RegistraNotificacion()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                gr = null;
            }
            return gr;
        }

        private GeneralResult ContenidoMail(ClienteSaldoVencido Cliente)
        {
            GeneralResult gr = new GeneralResult();
            DataTable dt = null;
            MailContent mc = new MailContent();
            string sRecursosImagen = string.Empty;
            string sRutaImagenes = string.Empty;
            List<string> archivosMail = new List<string>();
            GeneralResult EstadoCuenta = new GeneralResult();
            GeneralResult RecursosImagenCorreo = new GeneralResult();
            List<RecursoImagen> lst_ri = new List<RecursoImagen>();
            List<string> imagen = new List<string>();
            RecursoImagen ri;
            try
            {
                using (AccesoDatos datos = new AccesoDatos(DBConnection.Eximagen_Electro))
                {
                    datos.AddParameterIn("@pMailConfig", SqlDbType.VarChar, 1);
                    dt = datos.ExecuteDataTable(SP_CMD_MAIL_CONTENT);
                }

                if (dt.Rows.Count > 0)
                {
                    mc.MailCfgID = Convert.ToInt32(dt.Rows[0]["MailConfigID"].ToString());
                    mc.MailFrom = dt.Rows[0]["MailFrom"].ToString();
                    mc.MailSubject = dt.Rows[0]["MailSubject"].ToString() + " " + Cliente.CardCode;
                    mc.HTMLBody = dt.Rows[0]["HTMLBody"].ToString();
                    sRecursosImagen = dt.Rows[0]["RecursoImagen"].ToString();
                    sRutaImagenes = dt.Rows[0]["RutaImagenes"].ToString();
                    mc.AddCCO = dt.Rows[0]["AddCCO"].ToString();
                    mc.AddReplyTo = dt.Rows[0]["AddReplyTo"].ToString();
                    imagen = sRecursosImagen.Split(';').ToList();

                    foreach(string si in imagen)
                    {
                        ri = new RecursoImagen();
                        ri.NombreImagen = si;
                        ri.RutaImagen = sRutaImagenes;
                        lst_ri.Add(ri);
                    }

                    RecursosImagenCorreo = ObtenerRecursoImagen(lst_ri);
                    if(RecursosImagenCorreo.TipoResultado == ResultType.Error){
                        throw new Exception("Error al Obtener recursos de imagen para el correo");
                    }

                    EstadoCuenta = GeneraEstadoCuenta(Cliente.CardCode);

                    if (EstadoCuenta.TipoResultado == ResultType.Exito)
                    {
                        archivosMail.Add((string)EstadoCuenta.DataResultado);

                        mc.MailTo = Cliente.EMail;
                        mc.ListaArchivos = archivosMail;
                        mc.ListaImagenes = (List<LinkedResource>)RecursosImagenCorreo.DataResultado;
                        gr.DataResultado = mc;
                        gr.CodigoResultado = 1;
                        gr.DescripcionResultado = "Datos de Mail Obtenidos";
                        gr.TipoResultado = ResultType.Exito;
                        mc = (MailContent)gr.DataResultado;
                    }
                    else
                    {
                        throw new Exception("Error al obtener Estado de Cuenta para cliente : " + Cliente.CardCode + " - " + Cliente.CardName);
                    }
                }
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "ContenidoMail()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                return gr;
            }
            return gr;
        }

        private GeneralResult ObtenerRecursoImagen(List<RecursoImagen> Recursos)
        {
            GeneralResult gr = new GeneralResult();
            LinkedResource lr;
            List<LinkedResource> lst_ri = new List<LinkedResource>();

            try
            {
                foreach(RecursoImagen ri in Recursos)
                {
                    lr = new LinkedResource(Path.Combine(ri.RutaImagen, ri.NombreImagen), System.Net.Mime.MediaTypeNames.Image.Jpeg);
                    lr.ContentId = ri.NombreImagen.Replace(".jpg", "");
                    lr.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    lst_ri.Add(lr);
                }


                gr.CodigoResultado = 1;
                gr.DataResultado = lst_ri;
                gr.DescripcionResultado = "Recusos de Imagen Agregados";
                gr.TipoResultado = ResultType.Exito;

            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "GeneraEstadoCuenta()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                return gr;
            }
            return gr;
        }
        private GeneralResult GeneraEstadoCuenta(string CardCode)
        {
            GeneralResult gr = new GeneralResult();
            GeneralResult datosPDF = new GeneralResult();
            GeneralResult exportaPDF = new GeneralResult();
            List<object> lst_oData = new List<object>();
            List<DatosSaldoCliente> lst_dsc = new List<DatosSaldoCliente>();
            List<EstadosSaldoCliente> lst_esc = new List<EstadosSaldoCliente>();
            List<TotalesSaldoCliente> lst_tsc = new List<TotalesSaldoCliente>();
            try
            {
                datosPDF = DataPDF(CardCode);
                if(datosPDF.TipoResultado == ResultType.Exito)
                {
                    lst_oData = (List<object>)datosPDF.DataResultado;

                    lst_dsc = (List<DatosSaldoCliente>)lst_oData[0];
                    lst_esc = (List<EstadosSaldoCliente>)lst_oData[1];
                    lst_tsc = (List<TotalesSaldoCliente>)lst_oData[2];
                    exportaPDF = DocumentExportToPDF(lst_dsc, lst_esc, lst_tsc);
                    if(exportaPDF.TipoResultado == ResultType.Exito)
                    {
                        gr.TipoResultado = ResultType.Exito;
                        gr.DescripcionResultado = "Generacion Estado de Cuenta Exitoso";
                        gr.CodigoResultado = 1;
                        gr.DataResultado = exportaPDF.DataResultado;
                    }
                    else
                    {
                        throw new Exception("No se Pudo generar Estado de Cuenta");
                    }
                }
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "GeneraEstadoCuenta()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                return gr;
            }
            return gr;
        }

        private GeneralResult DataPDF(string CardCode)
        {
            GeneralResult gr = new GeneralResult();
            DataTable dt = null;

            List<object> lst_oData = new List<object>();

            DatosSaldoCliente dsc;
            List<DatosSaldoCliente> lst_dsc = new List<DatosSaldoCliente>();

            EstadosSaldoCliente esc;
            List<EstadosSaldoCliente> lst_esc = new List<EstadosSaldoCliente>();

            TotalesSaldoCliente tsc;
            List<TotalesSaldoCliente> lst_tsc = new List<TotalesSaldoCliente>();

            try
            {
                using (AccesoDatos data = new AccesoDatos(DBConnection.Eximagen_SAP))
                {
                    data.AddParameterIn("@Id_Cliente", SqlDbType.NVarChar, CardCode);
                    dt = data.ExecuteDataTable(SP_CMD_DATOS_SALDO_CLIENTE);
                }

                if(dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        dsc = new DatosSaldoCliente(){
                            CardName = dr["cardname"].ToString(),
                            Fecha = dr["fecha"].ToString(),
                            CreditLine = Convert.ToString(dr["creditline"].ToString()),
                            Balance = Convert.ToString(dr["balance"].ToString()),
                            Dispo = Convert.ToString(dr["dispo"].ToString())
                        };
                        lst_dsc.Add(dsc);
                    }
                    lst_oData.Add(lst_dsc);
                    dt = null;
                }

                using (AccesoDatos data = new AccesoDatos(DBConnection.Eximagen_SAP))
                {
                    data.AddParameterIn("@Id_Cliente", SqlDbType.NVarChar, CardCode);
                    dt = data.ExecuteDataTable(SP_CMD_ESTADOS_SALDO_CLIENTE);
                }

                if(dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        esc = new EstadosSaldoCliente(){
                            TransId = Convert.ToInt64(dr["TransId"]),
                            TransRowId = Convert.ToInt32(dr["TransRowId"]),
                            Cliente = Convert.ToString(dr["CLIENTE"]),
                            Tipo = Convert.ToString(dr["TIPO"]),
                            CreatedBy = Convert.ToInt64(dr["CreatedBy"]),
                            Folio = Convert.ToString(dr["Folio"]),
                            Total = Convert.ToString(dr["Total"]),
                            SourceLine = Convert.ToInt32(dr["SourceLine"]),
                            FechaEmi = Convert.ToString(dr["fecha_emi"]),
                            FechaVen = Convert.ToString(dr["FECHA_VEN"]),
                            SaldoVencido = Convert.ToString(dr["saldo_vencido"]),
                            Dias = Convert.ToInt32(dr["Dias"]),
                            AbonoFuturo = Convert.ToString(dr["AbonoFuturo"]),
                            p_0_7 = Convert.ToString(dr["0-7"].ToString()),
                            p_8_15 = Convert.ToString(dr["8-15"].ToString()),
                            p_16_30 = Convert.ToString(dr["16-30"].ToString()),
                            p_31_60 = Convert.ToString(dr["31-60"].ToString()),
                            p_61_90 = Convert.ToString(dr["61-90"].ToString()),
                            p_90_more = Convert.ToString(dr["90+"].ToString())                            
                        };
                        lst_esc.Add(esc);
                    }
                    lst_oData.Add(lst_esc);
                    dt = null;
                }

                using (AccesoDatos data = new AccesoDatos(DBConnection.Eximagen_SAP))
                {
                    data.AddParameterIn("@Id_Cliente", SqlDbType.NVarChar, CardCode);
                    dt = data.ExecuteDataTable(SP_CMD_TOTALES_SALDO_CLIENTE);
                }

                if(dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        tsc = new TotalesSaldoCliente(){
                            TotalSaldo = Convert.ToString(dr["TotalSaldo"]),
                            TotalAbonoFuturo = Convert.ToString(dr["TotalAbonoFuturo"]),
                            p_0_7 = Convert.ToString(dr["0-7"]),
                            p_8_15 = Convert.ToString(dr["8-15"]),
                            p_16_30 = Convert.ToString(dr["16-30"]),
                            p_31_60 = Convert.ToString(dr["31-60"]),
                            p_61_90 = Convert.ToString(dr["61-90"]),
                            p_90_more = Convert.ToString(dr["90+"])                            
                        };
                        lst_tsc.Add(tsc);
                    }
                    lst_oData.Add(lst_tsc);
                    dt = null;
                }

                gr.CodigoResultado = 1;
                gr.DataResultado = "Datos Saldos Cliente Obtenidos";
                gr.TipoResultado = ResultType.Exito;
                gr.DataResultado = lst_oData;

            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "DataPDF()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            return gr;
        }

        private GeneralResult DocumentExportToPDF(List<DatosSaldoCliente> lstDatos, List<EstadosSaldoCliente> lstEstado, List<TotalesSaldoCliente> lstTotales)
        {
            GeneralResult gr = new GeneralResult();
            string strHTML = string.Empty;
            Byte[] bytes;
            //var doc = new Document();
            MemoryStream ms = new MemoryStream();
            //var writer = PdfWriter.GetInstance(doc, ms);
            try
            {

                #region HTMLFormat
                strHTML = "<html>";
                strHTML = strHTML + "<body>";
                strHTML = strHTML + "<table bordercolor='#e6e6e6'>";
                strHTML = strHTML + "  <tr>";
                strHTML = strHTML + "    <td><div align='center'><img src='http://www2.promoshop.com.mx/Reporte/IMAGES/logo_exi_solo.gif' width='150' height='23'> </div></td>";
                strHTML = strHTML + "    <td><div align='center' style=''>&quot;Hacemos que tu marca se vea mejor y siempre la recuerden&quot;</div></td>";
                strHTML = strHTML + "    <td><div align='center'><img src='http://www2.promoshop.com.mx/Reporte/IMAGES/promo.gif' width='131' height='26'></div></td>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "</table>";

                strHTML = strHTML + "<table width='100%' border='0.1' align='center' bgcolor='#CCCCCC' bordercolor='#e6e6e6'>";
                strHTML = strHTML + "  <tr bgcolor='#006699'>";
                strHTML = strHTML + "    <th height='27' colspan='10' align='center' valign='middle' bgcolor='#FFFFFF'><strong class='aspmaker' style='color: #221F73'>Estado de Adeudos de Cliente";
                strHTML = strHTML + "    </strong> </th>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "  <tr bgcolor='#006699'>";
                strHTML = strHTML + "    <td colspan='2' align='center' valign='middle' bgcolor='#3366CC' width='200px'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Cliente</b></font></td>";
                strHTML = strHTML + "    <th colspan='18' align='center' valign='middle' bgcolor='#FFFFFF'> ";
                strHTML = strHTML + "	<font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>";
                strHTML = strHTML + lstDatos[0].CardName.ToString();
                strHTML = strHTML + "</b></font></th>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "  <tr bgcolor='#006699'>";
                strHTML = strHTML + "    <td colspan='2' align='center' valign='middle' bgcolor='#3366CC' width='200px'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Fecha</b></font></td>";
                strHTML = strHTML + "    <th colspan='18' align='center' valign='middle' bgcolor='#FFFFFF'>";
                strHTML = strHTML + "	<font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>";
                strHTML = strHTML + lstDatos[0].Fecha.ToString();
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "  <tr bgcolor='#006699'>";
                strHTML = strHTML + "    <td colspan='2' align='center' valign='middle' bgcolor='#3366CC' width='200px'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>L&iacute;nea de Cr&eacute;dito:</b></font></td>";
                strHTML = strHTML + "    <th colspan='18' align='center' valign='middle' bgcolor='#FFFFFF'>";
                strHTML = strHTML + "	<font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>";
                strHTML = strHTML + lstDatos[0].CreditLine.ToString();
                strHTML = strHTML + "	</b></font></th>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "  <tr bgcolor='#006699'>";
                strHTML = strHTML + "    <td colspan='2' align='center' valign='middle' bgcolor='#3366CC' width='200px'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Saldo Total: </b></font></td>";
                strHTML = strHTML + "    <th colspan='18' align='center' valign='middle' bgcolor='#FFFFFF'>";
                strHTML = strHTML + "	<font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>";
                strHTML = strHTML + lstDatos[0].Balance.ToString();
                strHTML = strHTML + "	</b></font>	</th>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "  <tr bgcolor='#006699'>";
                strHTML = strHTML + "    <td colspan='2' align='center' valign='middle' bgcolor='#3366CC' width='200px'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Cr&eacute;dito Disponible: </b></font></td>";
                strHTML = strHTML + "    <th colspan='18' align='center' valign='middle' bgcolor='#FFFFFF'>";
                strHTML = strHTML + "	<font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>";
                strHTML = strHTML + lstDatos[0].Dispo.ToString();
                strHTML = strHTML + "	</b></font>	</th>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + "</table>";

                strHTML = strHTML + " <table width='100%' bgcolor='#3366CC' color='#FFFFFF' border='0.1'><tr><td colspan='7'></td><td align='center' colspan='6'>D&iacute;as Vencidos</td></tr></table>";

                strHTML = strHTML + "<table bgcolor='#3366CC' color='#FFFFFF' border='0.1' bordercolor='#e6e6e6' style='width:100%'>";
                strHTML = strHTML + "<tr>";
                strHTML = strHTML + "  <td  align='center'>Folio</td>";
                strHTML = strHTML + "  <td  align='center'>Tipo</td>";
                strHTML = strHTML + "  <td  align='center'>Fecha Emisi&oacute;n </td>";
                strHTML = strHTML + "  <td  align='center'>Fecha Vencimiento</td>";
                strHTML = strHTML + "  <td  align='center'>Importe </td>";
                strHTML = strHTML + "  <td  align='center'>Saldo </td>";
                strHTML = strHTML + "  <td  align='center'>Abono Futuro </td>";
                strHTML = strHTML + "  <td align='center' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>0-7</b></font></td>";
                strHTML = strHTML + "  <td align='center' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>8-15</b></font></td>";
                strHTML = strHTML + "  <td align='center' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>16-30</b></font></td>";
                strHTML = strHTML + "  <td align='center' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>31-60</b></font></td>";
                strHTML = strHTML + "  <td align='center' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>61-90</b></font></td>";
                strHTML = strHTML + "  <td align='center' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>91+</b></font></td>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + " </table>";

                strHTML = strHTML + " <table  width='100%' border='0.1' align='center'  bgcolor='#CCCCCC' bordercolor='#e6e6e6'>";

                for (int i = 0; lstEstado.Count() > i; i++)
                {

                    strHTML = strHTML + "<tr bgcolor='#F5F5F5'>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='center'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'>" + lstEstado[i].Folio + "</font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'>";
                    strHTML = strHTML + lstEstado[i].Tipo;
                    strHTML = strHTML + "	  </font></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'>" + lstEstado[i].FechaEmi + "</font></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstEstado[i].FechaVen + "</b></font></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'> " + lstEstado[i].Total + " </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'> " + lstEstado[i].SaldoVencido + " </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].AbonoFuturo;
                    strHTML = strHTML + "      </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].p_0_7;
                    strHTML = strHTML + "      </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].p_8_15;
                    strHTML = strHTML + "      </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].p_16_30;
                    strHTML = strHTML + "      </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].p_31_60;
                    strHTML = strHTML + "      </font></div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].p_61_90;
                    strHTML = strHTML + "      </font> </div></td>";
                    strHTML = strHTML + "      <td align='center' valign='middle' bgcolor='#FFFFFF'><div align='right'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>";
                    strHTML = strHTML + lstEstado[i].p_90_more;
                    strHTML = strHTML + "      </font> </div></td>";
                    strHTML = strHTML + "</tr>";
                }
                strHTML = strHTML + "  </table >";

                strHTML = strHTML + "<table bgcolor='#3366CC' color='#FFFFFF' border='1' bordercolor='#e6e6e6' style='width:100%'>";
                strHTML = strHTML + "<tr>";
                strHTML = strHTML + "  <td align='center' colspan='5'>TOTAL VENCIDO</td>";
                //strHTML = strHTML + "  <td align='center'></td>";
                //strHTML = strHTML + "  <td align='center'></td>";
                //strHTML = strHTML + "  <td align='center'></td>";
                //strHTML = strHTML + "  <td align='center'></td>";
                strHTML = strHTML + "  <td align='center'>" + lstTotales[0].TotalSaldo.ToString() + "</td>";
                strHTML = strHTML + "  <td align='right'>" + lstTotales[0].TotalAbonoFuturo.ToString() + "</td>";
                strHTML = strHTML + "  <td align='right' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstTotales[0].p_0_7.ToString() + "</b></font></td>";
                strHTML = strHTML + "  <td align='right' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstTotales[0].p_8_15.ToString() + "</b></font></td>";
                strHTML = strHTML + "  <td align='right' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstTotales[0].p_16_30.ToString() + "</b></font></td>";
                strHTML = strHTML + "  <td align='right' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstTotales[0].p_31_60.ToString() + "</b></font></td>";
                strHTML = strHTML + "  <td align='right' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstTotales[0].p_61_90.ToString() + "</b></font></td>";
                strHTML = strHTML + "  <td align='right' valign='middle' bgcolor='#3366CC'><font color='#FFFFFF' face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>" + lstTotales[0].p_90_more.ToString() + "</b></font></td>";
                strHTML = strHTML + "  </tr>";
                strHTML = strHTML + " </table>";


                strHTML = strHTML + "</body>";
                strHTML = strHTML + "</html >";

                #endregion

                //doc.Open();
                //var htmlWorker = new iTextSharp.text.html.simpleparser.HTMLWorker(doc);
                //using (var sr = new StringReader(strHTML))
                //{
                //    htmlWorker.Parse(sr);
                //}

                //doc.Close();

                //bytes = ms.ToArray();


                StringReader sr = new StringReader(strHTML);
                Document pdfDoc = new Document();
                pdfDoc.SetPageSize(PageSize.LETTER.Rotate());
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                PdfWriter.GetInstance(pdfDoc,ms);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();

                bytes = ms.ToArray();

                if(!Directory.Exists(_pdfDirectory))
                {
                    Directory.CreateDirectory(_pdfDirectory);
                }
                string pdfFileName = lstDatos[0].CardName.Replace("/", "_") + ".pdf";
                var pdfFile = Path.Combine(_pdfDirectory, pdfFileName);
                System.IO.File.WriteAllBytes(pdfFile, bytes);

                gr.CodigoResultado = 1;
                gr.DescripcionResultado = "PDF Generado " + lstDatos[0].CardName;
                gr.DataResultado = Path.Combine(_pdfDirectory, pdfFileName);
                gr.TipoResultado = ResultType.Exito;
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "DocumentExportToPDF()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
                return gr;
            }
            return gr;
        }

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

        }

        public void Dispose()
        {

            Dispose(true);

            GC.SuppressFinalize(this);

        }

        ~Saldos()
        {
            Dispose(false);
        }

        #endregion
    }
}
