﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class ClienteSaldoVencido
    {
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string EMail { get; set; }
        public double Saldo { get; set; }
        public DateTime FechaVerificacion { get; set; }
        public string RutaArchivoEstadoCuenta { get; set; }
    }
}
