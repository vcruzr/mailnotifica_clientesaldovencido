﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class DatosSaldoCliente
    {
        public string CardName { get; set; }
        public string Fecha { get; set; }
        public string CreditLine { get; set; }
        public string Balance { get; set; }
        public string Dispo { get; set; }
    }
}
