USE [Eximagen_NuevaB]
GO
/****** Object:  StoredProcedure [dbo].[SP_TotalesEstadoAdeudosCliente]    Script Date: 07/03/2019 05:28:35 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TotalesEstadoAdeudosCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_TotalesEstadoAdeudosCliente]
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosAdeudosCliente]    Script Date: 07/03/2019 05:28:35 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_EstadosAdeudosCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_EstadosAdeudosCliente]
GO
/****** Object:  StoredProcedure [dbo].[SP_DatosAdeudosCliente]    Script Date: 07/03/2019 05:28:35 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DatosAdeudosCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_DatosAdeudosCliente]
GO
/****** Object:  StoredProcedure [dbo].[SP_DatosAdeudosCliente]    Script Date: 07/03/2019 05:28:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oscar Salazar>
-- Create date: <30/01/2019>
-- Description:	<Obtine los datos de cabecera de los estados de adeudos del cliente>
-- =============================================
CREATE PROCEDURE [dbo].[SP_DatosAdeudosCliente]
(
	@Id_Cliente NVARCHAR(15)
)
AS
BEGIN
	
SET LANGUAGE 'spanish'
SELECT 
	cardname, 
		DATENAME(DW, GETDATE()) + ' ' + DATENAME(day, GETDATE()) + ' de ' + DateName(month,GETDATE()) + ' del ' +DateName(year,GETDATE()) fecha, 
		'$' + CONVERT(VARCHAR, CAST( creditline AS MONEY),1) creditline,
		'$' + CONVERT(VARCHAR, CAST( balance AS MONEY),1) balance,
		'$' + CONVERT(VARCHAR, CAST( (creditline - balance) AS MONEY),1) dispo 
	FROM ocrd 
	WHERE cardcode = @Id_Cliente
END
GO
/****** Object:  StoredProcedure [dbo].[SP_EstadosAdeudosCliente]    Script Date: 07/03/2019 05:28:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oscar Salazar>
-- Create date: <30/01/2019>
-- Description:	<obtiene los estados de adeudos del cliente>
-- =============================================
CREATE PROCEDURE [dbo].[SP_EstadosAdeudosCliente]
(
	@Id_Cliente NVARCHAR(15)
)
AS
BEGIN

DECLARE @Fechaini DATE = GETDATE()
	
	SELECT 
			T1.[TransId], 
			T1.[TransRowId], 
			MAX(T0.[ShortName]) AS CLIENTE,
			--MAX(T0.[TransType])AS TIPO, 

				  CASE 
				  WHEN MAX(T0.[TransType]) = 30 THEN 'Registro'
				  WHEN MAX(T0.[TransType]) = 13 THEN 'Factura'
				  WHEN MAX(T0.[TransType]) = 14 THEN 'Nota de Crédito'
				  WHEN MAX(T0.[TransType]) = 24 THEN 'Factura'
				  WHEN MAX(T0.[TransType]) = 203 THEN 'Factura AN'
				  ELSE MAX(T0.[TransType]) END as TIPO,

			MAX(T0.[CreatedBy])as CreatedBy,
			MAX(T4.[U_Folio])as Folio, 
			CONVERT(VARCHAR, CAST( MAX(T4.[Doctotal]) AS MONEY),1) as Total, 
			MAX(T0.[SourceLine]) as SourceLine, 
			CONVERT(VARCHAR(10), MAX(T0.[RefDate]), 103)   as fecha_emi, 
			CONVERT(VARCHAR(10), MAX(T0.[DueDate]), 103)  AS FECHA_VEN, 
			--MAX(T0.[RefDate]) fecha_emi,
			--MAX(T0.[DueDate]) FECHA_VEN,
			CONVERT(VARCHAR, CAST( SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) AS MONEY),1) as saldo_vencido,
			convert(numeric,(@Fechaini)-(MAX(T0.[DueDate])))as Dias,
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) < 0 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1) AbonoFuturo,
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  0 AND 7 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END   AS MONEY),1) [0-7],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  8 AND 15 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END  AS MONEY),1) [8-15],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  16 AND 30 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1) [16-30],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  31 AND 60 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1) [31-60],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  61 AND 90 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1) [61-90],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) >= 91 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END              AS MONEY),1) [90+]


		FROM  [dbo].[JDT1] T0  
		INNER  JOIN [dbo].[ITR1] T1  ON  T1.[TransId] = T0.[TransId]  AND  T1.[TransRowId] = T0.[Line_ID]   
		INNER  JOIN [dbo].[OITR] T2  ON  T2.[ReconNum] = T1.[ReconNum]   
		INNER  JOIN [dbo].[OJDT] T3  ON  T3.[TransId] = T0.[TransId] 
		INNER JOIN [dbo].[OINV] T4 ON T4.[DOCNUM] = T0.[BASEREF]   
		WHERE 
			T0.[RefDate] <= (@Fechaini)  
			AND  T2.[ReconDate] > (@Fechaini)  
			AND T1.[IsCredit] = ('C')  
			AND  T0.[ShortName] IN(@Id_Cliente)    
		GROUP BY 
			T1.[TransId], T1.[TransRowId],T4.[U_Folio],T4.[Doctotal] 
		HAVING MAX(T0.[BalFcCred]) <>- SUM(T1.ReconSumFC)  OR  
			MAX(T0.[BalDueCred]) <>- SUM(T1.ReconSum)   
		UNION 
		SELECT 
			T1.[TransId], 
			T1.[TransRowId], 
			MAX(T0.[ShortName]), 
			--MAX(T0.[TransType])AS TIPO, 

				  CASE 
				  WHEN MAX(T0.[TransType]) = 30 THEN 'Registro'
				  WHEN MAX(T0.[TransType]) = 13 THEN 'Factura'
				  WHEN MAX(T0.[TransType]) = 14 THEN 'Nota de Crédito'
				  WHEN MAX(T0.[TransType]) = 24 THEN 'Factura'
				  WHEN MAX(T0.[TransType]) = 203 THEN 'Factura AN'
				  ELSE MAX(T0.[TransType]) END as TIPO, 
			MAX(T0.[CreatedBy]),
			MAX(T4.[U_Folio])as Folio, 
			CONVERT(VARCHAR, CAST( MAX(T4.[Doctotal]) AS MONEY),1) as Total, 
			MAX(T0.[SourceLine]), 
			CONVERT(VARCHAR(10), MAX(T0.[RefDate]), 103)   as fecha_emi, 
			CONVERT(VARCHAR(10), MAX(T0.[DueDate]), 103)  AS FECHA_VEN, 
			-- FORMAT( MAX(T0.[RefDate]), 'dd/MM/yyyy'), 
			-- FORMAT( MAX(T0.[DueDate]), 'dd/MM/yyyy'), 
			--MAX(T0.[RefDate]),  
			--MAX(T0.[DueDate]), 
			CONVERT(VARCHAR, CAST( - SUM(T1.[ReconSum])- MAX(T0.[BalDueDeb]) AS MONEY),1),
			convert(numeric,(@Fechaini)-(MAX(T0.[DueDate])))as Dias,
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) < 0 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)  AbonoFuturo,			
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  0 AND 7 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)   [0-7],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  8 AND 15 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)   [8-15],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  16 AND 30 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)  [16-30],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  31 AND 60 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)  [31-60],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN  61 AND 90 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)  [61-90],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) >= 91 THEN SUM(T1.[ReconSum])+ MAX(T0.[BalDueCred]) ELSE 0 END AS MONEY),1)   [90+]
		FROM  [dbo].[JDT1] T0  
			INNER  JOIN [dbo].[ITR1] T1  ON  T1.[TransId] = T0.[TransId]  AND  T1.[TransRowId] = T0.[Line_ID]   
			INNER JOIN [dbo].[OITR] T2  ON  T2.[ReconNum] = T1.[ReconNum]   
			INNER JOIN [dbo].[OJDT] T3  ON  T3.[TransId] = T0.[TransId]  
			INNER JOIN [dbo].[OINV] T4 ON T4.[DOCNUM] = T0.[BASEREF] 
		WHERE 
			T0.[RefDate] <= (@Fechaini)  
			AND  T2.[ReconDate] > (@Fechaini)  
			AND  T1.[IsCredit] = ('D')  
			AND  T0.[ShortName] IN(@Id_Cliente)    
		GROUP BY 
			T1.[TransId], T1.[TransRowId] ,T4.[U_Folio], T4.[Doctotal] 
		HAVING MAX(T0.[BalFcDeb]) <>- SUM(T1.ReconSumFC)  OR  MAX(T0.[BalDueDeb]) <>- SUM(T1.ReconSum)   
		UNION 
		SELECT 
			T0.[TransId], 
			T0.[Line_ID], 
			MAX(T0.[ShortName]), 
			--MAX(T0.[TransType])AS TIPO, 

				  CASE 
				  WHEN MAX(T0.[TransType]) = 30 THEN 'Registro'
				  WHEN MAX(T0.[TransType]) = 13 THEN 'Factura'
				  WHEN MAX(T0.[TransType]) = 14 THEN 'Nota de Crédito'
				  WHEN MAX(T0.[TransType]) = 24 THEN 'Factura'
				  WHEN MAX(T0.[TransType]) = 203 THEN 'Factura AN'
				  ELSE MAX(T0.[TransType]) END as TIPO,
			MAX(T0.[CreatedBy]), 
			CASE WHEN T0.[TransType] = 13 THEN T4.[DocNum] 
			WHEN T0.[TransType] = 14 THEN T5.[DocNum] END AS Folio, 
			CONVERT(VARCHAR, CAST( CASE WHEN T0.[TransType] = 13 THEN T4.[DocTotal] WHEN T0.[TransType] = 14 THEN T5.[DocTotal]*-1 END AS MONEY),1) AS Total , 
			MAX(T0.[SourceLine]), 
			CONVERT(VARCHAR(10), MAX(T0.[RefDate]), 103)   as fecha_emi, 
			CONVERT(VARCHAR(10), MAX(T0.[DueDate]), 103)  AS FECHA_VEN, 
			 --FORMAT( MAX(T0.[RefDate]),'dd/MM/yyyy'), 
			 --FORMAT( MAX(T0.[DueDate]),'dd/MM/yyyy'), 
			--MAX(T0.[RefDate]), 
			--MAX(T0.[DueDate]), 
			CONVERT(VARCHAR, CAST( CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END AS MONEY),1), 
			convert(numeric,(@Fechaini)-(MAX(T0.[DueDate])))as Dias,
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) < 0 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1) AbonoFuturo,
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN 0 AND 7 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1) [0-7],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN 8 AND 15 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1) [8-15],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN 16 AND 30 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1) [15-30],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN 31 AND 60 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1) [31-60],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) BETWEEN 61 AND 90 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1) [61-90],
			CONVERT(VARCHAR, CAST( CASE WHEN convert(numeric,(@Fechaini)-(MAX(T0.[DueDate]))) >= 91 THEN CASE WHEN T0.[TransType] = 13 THEN T0.[BalDueDeb]-T0.[BalDueCred] WHEN T0.[TransType] = 14 THEN (T0.[BalDueDeb])-(T0.[BalDueCred]) END ELSE 0 END AS MONEY),1)  [90+]		
		FROM  [dbo].[JDT1] T0  
			INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId] 
			LEFT JOIN [dbo].[OINV] T4 ON T4.[DOCNUM] = T0.[BASEREF] 
			LEFT JOIN [dbo].[ORIN] T5 ON T5.[DOCNUM] = T0.[BASEREF]  
		WHERE 
			T0.[RefDate] <= (@Fechaini) 
			AND T0.[TransType] in (13, 14)  AND   
			NOT EXISTS (
				SELECT U0.[TransId], U0.[TransRowId] 
				FROM  [dbo].[ITR1] U0  
				INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]  
				WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] >  (@Fechaini)   
				GROUP BY U0.[TransId], U0.[TransRowId])  
			AND  T0.[ShortName] IN(@Id_Cliente) 
		GROUP BY 
			T0.[TransId], T0.[Line_ID] ,T4.[U_Folio],T4.[Doctotal],T4.DocNum ,T5.DocNum,T4.DocTotal,T5.DocTotal,t0.TransType,T0.[BalDueDeb],T0.[BalDueCred]   
		HAVING MAX(T0.[BalFcCred]) <> MAX(T0.BalFcDeb)  OR  MAX(T0.[BalDueCred]) <> MAX(T0.BalDueDeb)   
		ORDER BY 3,9,5


END
GO
/****** Object:  StoredProcedure [dbo].[SP_TotalesEstadoAdeudosCliente]    Script Date: 07/03/2019 05:28:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oscar Salazar>
-- Create date: <30/01/2019>
-- Description:	<Obtiene los totales de los estados de adeudos del cliente>
-- =============================================
CREATE PROCEDURE [dbo].[SP_TotalesEstadoAdeudosCliente]
(
	 @Id_Cliente VARCHAR(15)
)
AS
BEGIN
	DECLARE @Fechaini DATE = GETDATE()

SELECT
	--******************--
	--*** TotalSaldo ***--
	--******************--
	'$' + ISNULL( CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]   
			WHERE 
				T0.[RefDate] <= (@FechaIni)  
				AND   NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]   
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@FechaIni)   
					GROUP BY U0.[TransId], U0.[TransRowId])  
				AND  T0.[ShortName] IN(@Id_Cliente)  
		) AS MONEY),1),'0') AS TotalSaldo,

	--************************--
	--*** TotalAbonoFuturo ***--
	--************************-- 
	'$' + ISNULL( CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]  
			WHERE T0.[RefDate] <= (@Fechaini)  AND   NOT EXISTS (
				SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
				INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]   
				WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini) 
				GROUP BY U0.[TransId], U0.[TransRowId])  
				AND  T0.[ShortName] IN(@Id_Cliente) 
				and convert(numeric,(@Fechaini)-(T0.[DueDate]))< 0
		) AS MONEY),1),'0' )AS TotalAbonoFuturo,

	--**************--
	--*** [0-7] ***--
	--**************--
	'$' + ISNULL(CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencidow 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]  
			WHERE T0.[RefDate] <= (@Fechaini)  
				AND   NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]  
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini)  
					GROUP BY U0.[TransId], U0.[TransRowId])  
					AND  T0.[ShortName] IN(@Id_Cliente)
				and convert(numeric,(@Fechaini)-(T0.[DueDate])) between 0 and 7
		) AS MONEY),1),'0') AS [0-7],

	--**************--
	--*** [8-15] ***--
	--**************--
	'$' + ISNULL(CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]   
			WHERE T0.[RefDate] <= (@Fechaini)  
				AND   NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]  
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini)   
					GROUP BY U0.[TransId], U0.[TransRowId])  AND  T0.[ShortName] IN(@Id_Cliente)
				and convert(numeric,(@Fechaini)-(T0.[DueDate])) between 8 and 15
		) AS MONEY),1),'0') AS [8-15],

	--***************--
	--*** [16-30] ***--
	--***************--
	'$' + ISNULL(CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]   
			WHERE T0.[RefDate] <= (@Fechaini)  
				AND   NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]   
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini)   
					GROUP BY U0.[TransId], U0.[TransRowId])  AND  T0.[ShortName] IN(@Id_Cliente) 
			and convert(numeric,(@Fechaini)-(T0.[DueDate])) between 16 and 30
		) AS MONEY),1),'0') AS [16-30],

	--***************--
	--*** [31-60] ***--
	--***************--
	'$' + ISNULL(CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]   
			WHERE T0.[RefDate] <= (@Fechaini)  
				AND   NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]   
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini)   
					GROUP BY U0.[TransId], U0.[TransRowId])  AND  T0.[ShortName] IN(@Id_Cliente) 
				and convert(numeric,(@Fechaini)-(T0.[DueDate])) between 31 and 60
		) AS MONEY),1),'0') AS [31-60],

	--**************--
	--*** [61-90 ***--
	--**************--
	'$' + ISNULL(CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  
				[dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]   
			WHERE T0.[RefDate] <= (@Fechaini)  
				AND   NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]   
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini)   
					GROUP BY U0.[TransId], U0.[TransRowId])  AND  T0.[ShortName] IN(@Id_Cliente) 
				and convert(numeric,(@Fechaini)-(T0.[DueDate])) between 61 and 90
		) AS MONEY),1),'0') AS [61-90],

	--*************--
	--*** [90+] ***--
	--*************--
	'$' + ISNULL(CONVERT( VARCHAR, CAST( (
			SELECT 
				SUM(T0.[BalDueDeb])-MAX(T0.[BalDueCred]) as saldo_vencido 
			FROM  [dbo].[JDT1] T0  
				INNER  JOIN [dbo].[OJDT] T1  ON  T1.[TransId] = T0.[TransId]
			WHERE T0.[RefDate] <= (@Fechaini)  
				AND  NOT EXISTS (
					SELECT U0.[TransId], U0.[TransRowId] FROM  [dbo].[ITR1] U0  
					INNER  JOIN [dbo].[OITR] U1  ON  U0.[ReconNum] = U1.[ReconNum]   
					WHERE T0.[TransId] = U0.[TransId]  AND  T0.[Line_ID] = U0.[TransRowId]  AND  U1.[ReconDate] > (@Fechaini)  
					GROUP BY U0.[TransId], U0.[TransRowId])  AND  T0.[ShortName] IN(@Id_Cliente) 
			and convert(numeric,(@Fechaini)-(T0.[DueDate])) between 91 and 300
		) AS MONEY),1),'0') AS [90+]


END
GO
