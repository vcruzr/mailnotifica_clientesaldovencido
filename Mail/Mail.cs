﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using MailNot_Saldos.Entidades;
using System.Configuration;
using MailNot_Saldos.Common;
using System.IO;


namespace MailNot_Saldos.eMail
{
    public class Mail: IDisposable
    {
        MailParametros _mp = new MailParametros();
        MailContent _mc = new MailContent();
        Log EventLog = new Log();
        string _Mensaje = string.Empty;
        const string _Clase = "Mail";
        bool bAmbienteProduccion = false; 
        public Mail()
        {
            try
            {
                ObtenerConfiguracion();
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "Mail()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
        }

        public GeneralResult EnviarMail(MailContent Content)
        {
            GeneralResult gr = new GeneralResult();
            MailMessage mail = new MailMessage();
            try
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = new MailMessage();

                smtpClient.Host = _mp.Host;
                smtpClient.Port = _mp.Port;
                smtpClient.UseDefaultCredentials = _mp.UseDefaultCredentials;
                smtpClient.EnableSsl = _mp.EnableSSL;
                smtpClient.Credentials = new System.Net.NetworkCredential(_mp.User, _mp.Passwd);
                message.Sender = new System.Net.Mail.MailAddress(Content.MailFrom);
                message.From = new System.Net.Mail.MailAddress(Content.MailFrom);
                message.Subject = Content.MailSubject;
                message.IsBodyHtml = true;
                message.Priority = System.Net.Mail.MailPriority.High;

                foreach (string item in Content.ListaArchivos)
                {
                    message.Attachments.Add(new Attachment(item));
                }

                //  PRUEBAS
                message.To.Add("victor.cruz@eximagen.com.mx");
                message.CC.Add("yaritza.bustos@eximagen.com.mx");

                // PRODUCCION
                //if (bAmbienteProduccion == true)
                //{
                //    message.To.Add(Content.MailTo);
                //    message.Bcc.Add(Content.AddCCO);
                //}
                //else 
                //{
                //message.To.Add(Content.AddCCO);
                //}

                //if (Content.AddReplyTo != "")
                //{
                //    message.ReplyToList.Add(new MailAddress(Content.AddReplyTo, ""));
                //}


                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(Content.HTMLBody, null, "text/html");
                foreach(LinkedResource lr in Content.ListaImagenes)
                {
                    htmlView.LinkedResources.Add(lr);
                }
                message.AlternateViews.Add(htmlView);

                smtpClient.Send(message);

                _Mensaje = "Envio de notificacion e-Mail Asunto: " + Content.MailSubject + " :" + Content.MailTo;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Evento, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "EnvarMail()", sMensaje = _Mensaje };
                gr.CodigoResultado = 1;
                gr.DescripcionResultado = "Se Envio Mail de Notificacion";
                gr.TipoResultado = ResultType.Exito;

            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "EnviarMail()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            return gr;
        }

        private GeneralResult ObtenerConfiguracion()
        {
            GeneralResult gr = new GeneralResult();
            MailContent mc;
            try
            {
                bAmbienteProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["EnviromentProduction"]);
                _mp = new MailParametros() {
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Host = Convert.ToString(ConfigurationManager.AppSettings["Host"]),
                    TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]),
                    EnableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]),
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["DefaultCredentials"]),
                    User = Convert.ToString(ConfigurationManager.AppSettings["User"]),
                    Passwd = Convert.ToString(ConfigurationManager.AppSettings["Password"]),
                    AliasFrom = Convert.ToString(ConfigurationManager.AppSettings["AliasFrom"])
                };
                return gr;
            }
            catch (Exception ex)
            {
                gr.TipoResultado = ResultType.Error; gr.DescripcionResultado = ex.Message; gr.CodigoResultado = -1;
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = _Clase, sMetodo = "ObtenerConfiguracion()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                gr = null;
            }
            return gr;
        }


        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

        }

        public void Dispose()
        {

            Dispose(true);

            GC.SuppressFinalize(this);

        }

        ~Mail()
        {
            Dispose(false);
        }

        #endregion
    }
}
