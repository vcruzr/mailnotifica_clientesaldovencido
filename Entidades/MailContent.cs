﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace MailNot_Saldos.Entidades
{
    public class MailContent
    {
        public int MailCfgID { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string MailSubject { get; set; }
        public string HTMLBody { get; set; }
        public List<string> ListaArchivos { get; set; }
        public List<LinkedResource> ListaImagenes { get; set; }
        public string AddCCO { get; set; }
        public string AddReplyTo { get; set; }
    }
}
