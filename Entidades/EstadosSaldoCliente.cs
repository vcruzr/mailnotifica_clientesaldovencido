﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class EstadosSaldoCliente
    {
        public Int64 TransId { get; set; }
        public int TransRowId { get; set; }
        public string Cliente { get; set; }
        public string Tipo { get; set; }
        public Int64 CreatedBy { get; set; }
        public string Folio { get; set; }
        public string Total { get; set; }
        public int SourceLine { get; set; }
        public string FechaEmi { get; set; }
        public string FechaVen { get; set; }
        public string SaldoVencido { get; set; }
        public int Dias { get; set; }
        public string AbonoFuturo { get; set; }
        public string p_0_7 { get; set; }
        public string p_8_15 { get; set; }
        public string p_16_30 { get; set; }
        public string p_31_60 { get; set; }
        public string p_61_90 { get; set; }
        public string p_90_more { get; set; }
    }
}
