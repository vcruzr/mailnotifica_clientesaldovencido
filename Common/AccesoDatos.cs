﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using MailNot_Saldos.Common;
using MailNot_Saldos.Entidades;

namespace MailNot_Saldos.Common
{
    public class AccesoDatos : IDisposable
    {
        string Mensaje = string.Empty;
        const string Clase = "AccesoDatos";
        const string Modulo = "MailNot_Saldos";
        private List<SqlParameter> lstParam = new List<SqlParameter>();
        private string sCnn = string.Empty;

        public AccesoDatos(DBConnection BaseDatos)
        {
            try
            {
                switch (BaseDatos)
                {
                    case DBConnection.Eximagen_SAP:
                        sCnn = ConfigurationManager.ConnectionStrings["eximagenSAP"].ToString();
                        break;
                    case DBConnection.Eximagen_Electro:
                        sCnn = ConfigurationManager.ConnectionStrings["eximagenElectro"].ToString();
                        break;
                }
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog(){ TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "AccesoDatos()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
        }

        public void AddParameterIn(string paramNombre, SqlDbType dbTipo, object Valor)
        {
            SqlParameter objParam = new SqlParameter();
            try
            {
                objParam.ParameterName = paramNombre;
                objParam.SqlDbType = dbTipo;
                objParam.Value = Valor;
                lstParam.Add(objParam);
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "AccesoDatos()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                objParam = null;
            }
        }

        private void AddParameterIn(SqlParameter objParam)
        {
            try
            {
                lstParam.Add(objParam);
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "AccesoDatos()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
        }

        public DataTable ExecuteDataTable(string sCommand)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand Cmd = new SqlCommand();
            SqlConnection SQLCnn;

            try
            {
                SQLCnn = new SqlConnection(sCnn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Connection = SQLCnn;
                Cmd.CommandTimeout = 300;
                Cmd.CommandText = sCommand;

                foreach (SqlParameter iParam in lstParam)
                {
                    Cmd.Parameters.Add(iParam);
                }

                if (SQLCnn.State == ConnectionState.Closed)
                {
                    SQLCnn.Open();
                }

                da.SelectCommand = Cmd;
                da.Fill(dt);
                SQLCnn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "AccesoDatos()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                dt = null;
                da = null;
                Cmd = null;
                SQLCnn = null;
            }
            return dt;
        }

        public DataSet ExecuteDataSet(string sCommand)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand Cmd = new SqlCommand();
            SqlConnection SQLCnn;
            try
            {
                SQLCnn = new SqlConnection(sCnn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Connection = SQLCnn;
                Cmd.CommandTimeout = 300;
                Cmd.CommandText = sCommand;

                foreach (SqlParameter iParam in lstParam)
                {
                    Cmd.Parameters.Add(iParam);
                }

                if (SQLCnn.State == ConnectionState.Closed)
                {
                    SQLCnn.Open();
                }

                da.SelectCommand = Cmd;
                da.Fill(ds);
                SQLCnn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "ExecuteDataSet()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                ds = null;
                da = null;
                Cmd = null;
                SQLCnn = null;
            }
            return ds;
        }

        public bool ExecuteQuery(string sCommand)
        {
            bool Exito = false;
            SqlCommand SqlCmd = new SqlCommand();
            SqlConnection SQLCnn;
            try
            {
                SQLCnn = new SqlConnection(sCnn);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = SQLCnn;
                SqlCmd.CommandTimeout = 300;
                SqlCmd.CommandText = sCommand;

                foreach (SqlParameter iParam in lstParam)
                {
                    SqlCmd.Parameters.Add(iParam);
                }

                if (SQLCnn.State == ConnectionState.Closed)
                {
                    SQLCnn.Open();
                }

                SqlCmd.ExecuteNonQuery();
                SQLCnn.Close();
                Exito = true;
                return Exito;
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "ExecuteQuery()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                SQLCnn = null;
                SqlCmd = null;
            }
            return Exito;
        }

        public bool ExecuteScalar(string sCommand)
        {
            bool Exito = false;
            SqlCommand SqlCmd = new SqlCommand();
            SqlConnection SQLCnn;

            try
            {
                SQLCnn = new SqlConnection(sCnn);

                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = SQLCnn;
                SqlCmd.CommandTimeout = 300;
                SqlCmd.CommandText = sCommand;

                foreach (SqlParameter iParam in lstParam)
                {
                    SqlCmd.Parameters.Add(iParam);
                }

                if (SQLCnn.State == ConnectionState.Closed)
                {
                    SQLCnn.Open();
                }

                SqlCmd.ExecuteScalar();
                SQLCnn.Close();
                Exito = true;
                return Exito;
            }
            catch (Exception ex)
            {
                DataLog dataLog = new DataLog() { TipoLog = TipoLog.Error, TipoArchivo = TipoArchivo.Xml, sClase = Clase, sMetodo = "ExecuteScalar()", sMensaje = "Error: " + ex.Message };
                using (Log EventLog = new Log()) { EventLog.WriteLog(dataLog); dataLog = null; }
            }
            finally
            {
                SqlCmd = null;
                SqlCmd = null;
            }
            return Exito;
        }

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

        }

        public void Dispose()
        {

            Dispose(true);

            GC.SuppressFinalize(this);

        }

        ~AccesoDatos()
        {
            Dispose(false);
        }

        #endregion

    }

    public enum DBConnection
    {
        Eximagen_SAP = 1,
        Eximagen_Electro = 2
    }

}
