﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailNot_Saldos.Entidades
{
    public class TotalesSaldoCliente
    {
        public string TotalSaldo { get; set; }
        public string TotalAbonoFuturo { get; set; }
        public string p_0_7 {get; set;}
        public string p_8_15 { get; set; }
        public string p_16_30 { get; set; }
        public string p_31_60 { get; set; }
        public string p_61_90 { get; set; }
        public string p_90_more { get; set; }
    }
}
